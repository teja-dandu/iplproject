//O(n) time | O(1)
function getMatchesPerYear(matches) {

    let matchesPerYear = {};

    for (let i = 0; i < matches.length; i++){
        let season = matches[i]['season'];
        if(matchesPerYear.hasOwnProperty(season)){
            matchesPerYear[season] += 1;
        }
        else {
            matchesPerYear[season] = 1;
        }

    }

    return matchesPerYear;
}
//O(n) time | O(1) space
function numberOfMatchesOwn(matches) {
    let wonPerTeamPerYear = {};
    for(let i = 0; i < matches.length; i++){
        let teamOwn = matches[i].winner;
        let season = matches[i].season;

        if(wonPerTeamPerYear.hasOwnProperty(teamOwn)){
            let team = wonPerTeamPerYear[teamOwn];
            if(team.hasOwnProperty(season)){
                team[season] += 1;
            }
            else{
                team[season] = 1;
            }
        }
        else{
            let ownYear = {};
            ownYear[season] = 1;
            wonPerTeamPerYear[teamOwn] = ownYear;
        }
    }
    return wonPerTeamPerYear;

}

// O(n^2) time | O(1) space
function extraRunsPerTeamAtYear2016(matches, deliveries) {
    let runsConcededByTeam = {};
    for(let i = 0; i < matches.length; i++){
        let season = matches[i].season;
        if(season == 2016){
            let matchId = matches[i].id;
            let matchedDeliveries = deliveries.filter(function (delivery) {
                return delivery.match_id == matchId;

            });
            for(let j = 0; j < matchedDeliveries.length; j++){
                let bowlingTeam = matchedDeliveries[j].bowling_team;
                let extraRuns = Number(matchedDeliveries[j].extra_runs);
                if(runsConcededByTeam.hasOwnProperty(bowlingTeam)){
                    runsConcededByTeam[bowlingTeam] += extraRuns;
                }
                else{
                    runsConcededByTeam[bowlingTeam] = extraRuns;

                }
            }
        }
    }
    return runsConcededByTeam;

}


//O(n^2) time | O(1) space
function economicalBowlersTopTen2015(deliveries) {
    let bowlers = deliveries.filter(function (matches) {
      return (parseInt(matches.match_id) >= 518 && parseInt(matches.match_id) <= 576);
    }).reduce(function (output, current) {
      let ex = parseInt(current.total_runs)
      if (output.hasOwnProperty(current.bowler)) {
        output[current.bowler]['runs'] += ex;
        output[current.bowler]['balls'] += 1;
      } else {
        output[current.bowler] = {
          "runs": ex,
          "balls": 1
        }
      }
      return output;
    }, {});
    let keys = Object.keys(bowlers).reduce(function (output, current) {
      output[current] = (bowlers[current].runs * 6 / bowlers[current].balls).toFixed(2);
      return output;
    }, {})
    let result = Object.entries(keys).sort(function (a, b) {
      return a[1] - b[1];

    }).slice(0, 10);

    return result; // this function is returning Array  only
  }
function numberOfTimesWonTossWonMatch(matches) {
    // body...
    let countWinner = {};
    const team = [...matches.map(match => match.team1)];

    let wonTossAndWonMatch = {};
    matches.forEach((match)=>{
        let wonMatch = match.winner;
        let wonToss = match.toss_winner;
        if(wonMatch === wonToss){
            if(wonTossAndWonMatch.hasOwnProperty(wonMatch)){
                wonTossAndWonMatch[wonMatch] += 1;
            }
            else{
                wonTossAndWonMatch[wonMatch] = 1;
            }
        }
    });
    return wonTossAndWonMatch;

}

function playerOfTheMatches(matches) {
    // body...
    const matchCount = matches.reduce((accumlator, curValue) =>{
        accumlator[curValue['player_of_match']] = (accumlator[curValue['player_of_match']] || 0) + 1;
        return accumlator;
    },{});
    return Object.keys(matchCount).reduce((result, player) => {
        result.push([player, matchCount[player]]);
        return result;
    }, []);
}

function superOverEconomy(deliveries) {

  let economyBowlers = deliveries.reduce((economyBowlers, match) => {

    if (economyBowlers.hasOwnProperty(match.bowler)) {

      if (match.is_super_over == '1') {
        economyBowlers[match.bowler].balls += 1;
        economyBowlers[match.bowler].runs += parseInt(match.total_runs);
        let run = economyBowlers[match.bowler].runs;
        let ball = economyBowlers[match.bowler].balls;
        economyBowlers[match.bowler].economy = (run / (ball / 6)).toFixed(2);
      }
    }
    else {
      if (match.is_super_over == '1') {
        economyBowlers[match.bowler] = { "bowler": match.bowler, "runs": parseInt(match.total_runs), "balls": 1, "economy": 0 };
      }
    }
    return economyBowlers;
  }, {})
  let economyOnly = Object.values(economyBowlers).sort((a, b) => { return a.economy - b.economy })
  result = economyOnly[0];
  return result;
};

function strikeRateOfBatsMan(matches, deliveries) {

    let topBatsmen = {};
    let matchYear = [];
    let final = {};
    let finalStrike={};

    let years = [...new Set(matches.map(match => Number(match.season)))].sort((a,b) => a-b);


    for (let index = 0; index < years.length; index++) {

        matches.filter((match) => {
            if (Number(match.season) === years[index]) {
                matchYear.push(Number(match.id));
            }
        });

        let start = matchYear[0];
        let end = matchYear[matchYear.length - 1];

        let strike = deliveries.filter((delivery) => {
            if (delivery.match_id >= start && delivery.match_id <= end) {
                return true;
            } else {
                return false;
            }
        });

        strike.forEach((delivery) => {
            let name = delivery.batsman;
            let totalRuns = delivery.batsman_runs;

            if (topBatsmen.hasOwnProperty(name)) {
                topBatsmen[name].runs += Number(totalRuns);
                topBatsmen[name].balls += 1;
            }
            else {
                topBatsmen[name] = {
                    runs: Number(totalRuns),
                    balls: 1,
                    strikerate: Number(1)
                }
            }
            if (delivery.extra_runs === 0) {
                topBowlers[name].balls += 1;
            }

        });
        for(let player in topBatsmen){
            let runs = topBatsmen[player].runs;
            let balls = topBatsmen[player].balls;
            let rate = (runs / balls)*100;
            finalStrike[player] = Number(rate.toFixed(2));
        }
        final[years[index]] = finalStrike;
    }
    return final;
}

function maxPlayerDismissedByAnotherPlayer(deliveries) {
    let playerDismissed = [];
    for (let i = 0; i < deliveries.length; i++) {
        if (deliveries[i]['player_dismissed'] !== "") {
            playerDismissed.push([deliveries[i]['batsman'], deliveries[i]['bowler']]);
        }
    }
    let playerCount = [];

    for (let players of playerDismissed) {
        let batsman = players[0];
        let bowler = players[1];
        let count = 0;
        for (let pair of playerDismissed) {
            if (pair[0] == batsman && pair[1] == bowler) {
                count = count + 1;
            }
        }
        playerCount.push(count);
    }
    let maxDismissel = playerCount.reduce((a, b) => Math.max(a, b));

    let indexArray = [];
    for (let i = 0; i < playerCount.length; i++) {
        if (playerCount[i] == maxDismissel) {
            indexArray.push(i);
        }
    }

    let result = {}

    for (let index of indexArray) {

        batsman = playerDismissed[index][0]
        bowler = playerDismissed[index][1]

        let key = (batsman + "," + bowler)

        if (result.hasOwnProperty(key)) {
            continue
        }
        else {
            result[key] = maxDismissel
        }

    }
    return result;

}
module.exports = {
    getMatchesPerYear,
    numberOfMatchesOwn,
    extraRunsPerTeamAtYear2016,
    economicalBowlersTopTen2015,
    numberOfTimesWonTossWonMatch,
    playerOfTheMatches,
    superOverEconomy,
    strikeRateOfBatsMan,
    maxPlayerDismissedByAnotherPlayer,

}
