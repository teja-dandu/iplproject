const fs = require("fs");
const csv = require("csvtojson");
const path = require("path");

const ipl = require("./ipl")

const csvFilePath=path.resolve(__dirname, "../data/matches.csv");
const deliveriesCsvFilePath=path.resolve(__dirname, "../data/deliveries.csv");
// const csv=require('csvtojson')
let matchesData = {};
let deliveriesData = {};
csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{
    // console.log(jsonObj);

    matchesData = jsonObj;

    // let matchesPerYear = ipl.getMatchesPerYear(jsonObj);
    // console.log(matchesPerYear);

    // let wonPerTeamPerYear = ipl.numberOfMatchesOwn(jsonObj);
    // console.log(wonPerTeamPerYear);
    csv()
    .fromFile(deliveriesCsvFilePath)
    .then((jsonObj1)=>{
        deliveriesData = jsonObj1;
        // console.log(matchesData.length);
        // console.log(deliveriesData);
        // console.log(matchesData, deliveriesData);
        // let runsConcederedByTeam = ipl.extraRunsPerTeamAtYear2016(matchesData, deliveriesData);
        // console.log(extraRunsPerTeamAtYear2016);

        const matchesPerYear = ipl.getMatchesPerYear(matchesData);
        fs.writeFileSync(path.resolve(__dirname, '../public/output/matchesPerYear.json'), JSON.stringify(matchesPerYear));

        // console.log(matchesPerYear);

        const wonPerTeamPerYear = ipl.numberOfMatchesOwn(matchesData);
        fs.writeFileSync(path.resolve(__dirname, '../public/output/wonPerTeamPerYear.json'), JSON.stringify(wonPerTeamPerYear));
        // console.log(wonPerTeamPerYear);

        const runsConcededByTeam = ipl.extraRunsPerTeamAtYear2016(matchesData, deliveriesData);
        fs.writeFileSync(path.resolve(__dirname, '../public/output/runsConcededByTeam.json'), JSON.stringify(runsConcededByTeam));

        // console.log(runsConcededByTeam);
        const getEconomicalBowlers = ipl.economicalBowlersTopTen2015(deliveriesData);
        fs.writeFileSync(path.resolve(__dirname, '../public/output/getEconomicalBowlers.json'), JSON.stringify(getEconomicalBowlers));
        // console.log(getEconomicalBowlers);


        // const wonTossAndWonMatch = ipl.numberOfTimesWonTossWonMatch(matchesData);
        // console.log(wonTossAndWonMatch);

        // const matchCount = ipl.playerOfTheMatches(matchesData);
        // console.log(matchCount);

        // const economyBowlers = ipl.superOverEconomy(deliveriesData);
        // console.log(economyBowlers);

        // const finalStrike = ipl.strikeRateOfBatsMan(matchesData, deliveriesData);
        // console.log(finalStrike);


        // const playerDismissed = ipl.maxPlayerDismissedByAnotherPlayer(deliveriesData);
        // console.log(playerDismissed);



    });
});
