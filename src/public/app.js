// console.log('hello,world!');

fetch('http://127.0.0.1:8080/output/runsConcededByTeam.json')
  .then(response => response.json())
  .then(data => generateGraph(data));

// fetch('http://192.168.43.138:8080/output/matchesPerYear.json')
//   .then(response => response.json())
//   .then(data => generateGraph(data));


function generateGraph(runsConcededByTeam) {
  // body...
  Highcharts.chart('container', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Extra Runs Conceeded By Team 2016',
    },
    xAxis: {
      categories: Object.keys(runsConcededByTeam),
      // crosshair: true
    },
    yAxis: {
      // min: 0,
      title: {
        text: 'Extra Runs Conceeded',
      }
    },

    series: [{
      name: '2016',
      data: Object.values(runsConcededByTeam),
    }]
  });
}

fetch('http://127.0.0.1:8080/output/matchesPerYear.json')
  .then(response => response.json())
  .then(data => generateGraph(data));

function generateGraph(matchesPerYear) {
  // body...
  Highcharts.chart('container1', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Matches Per Year',
    },
    xAxis: {
      categories: Object.keys(matchesPerYear),
      // crosshair: true
    },
    yAxis: {
      // min: 0,
      title: {
        text: 'Matches Per Year',
      }
    },

    series: [{
      name: 'Matches',
      data: Object.values(matchesPerYear),
    }]
  });
}

fetch('http://127.0.0.1:8080/output/getEconomicalBowlers.json')
  .then(response => response.json())
  .then(data => generateGraph(data));
function generateGraph(getEconomicalBowlers) {
  // body...
  Highcharts.chart('container2', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'BestEconomicalBowlers',
    },
    xAxis: {
      categories: ["RN ten Doeschate",
      "J Yadav",
      "V Kohli",
      "R Ashwin",
      "S Nadeem",
      "Z Khan",
      "Parvez Rasool",
      "MC Henriques",
      "MA Starc",
      "M de Lange",],
      // crosshair: true
    },
    yAxis: {
      // min: 0,
      title: {
        text: 'Economy',
      }
    },

    series: [{
      name: 'Economy',
      data: [3.43,
      4.14,
      5.45,
      5.72,
      5.86,
      6.15,
      6.20,
      6.27,
      6.75,
      6.92],
    }]
  });
}

fetch('http://127.0.0.1:8080/output/wonPerTeamPerYear.json')
  .then(response => response.json())
  .then(data => generateGraph(data));

function generateGraph(wonPerTeamPerYear) {
  // body...
  Highcharts.chart('container3', {
    chart: {
      type: 'line',
    },
    title: {
      text: 'Matches Won Per Team Per Year',
    },
    xAxis: {
      categories: [
      2008,
      2009,
      2010,
      2011,
      2012,
      2013,
      2014,
      2015,
      2016,
      2017
      ],
      // crosshair: true
    },
    yAxis: {
      // min: 0,
      title: {
        text: 'won Year',
      }
    },

    series: [{
      name: "Sunrisers Hyderabad",
      data: [10,  6, 7, 11, 8],    // for SRH
    },{
      name: "Rising Pune Supergiant",
      data: [10],  // similarly for all other teams
    },{
      name: "Kolkata Knight Riders",
      data:[6, 3, 7, 8, 12, 6, 11, 7, 8, 9],
    },{
      name: "Kings XI Punjab",
      data: [10, 7, 4, 7, 8, 8, 12, 3, 4, 7],
    },{
      name: "Royal Challengers Bangalore",
      data: [4, 9, 8, 10, 8, 9, 5, 8, 9, 3],
    },{
      name: "Mumbai Indians",
      data: [10, 15, 11, 10, 10, 13, 7, 10, 7, 12],
    },{
      name: "Delhi Daredevils",
      data: [7, 10, 7, 4, 11, 3, 2, 5, 7, 6],
    },{
      name: "Gujarat Lions",
      data: [9, 4],
    },{
      name: "Chennai Super Kings",
      data: [9, 8, 9, 11, 10, 12, 10, 10],
    },{
      name: "Rajasthan Royals",
      data: [13, 6, 6, 6, 7, 11, 7, 7],
    },{
      name: "Deccan Chargers",
      data: [2, 9, 8, 6, 4],
    },{
      name: "Pune Warriors",
      data: [4, 4, 4],
    },{
      name: "Kochi Tuskers Kerala",
      data: [6],
    },{
      name: '',
      data: [1, 2],
    },{
      name: "Rising Pune Supergiants",
      data: [5]
    }]
  });
}
